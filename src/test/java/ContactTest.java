import dao.ContactListDAO;
import dao.DAOFactory;
import domain.Contact;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ContactService;
import service.ContactServiceImpl;
import sql.MySQLDAOFactory;
import sql.StatementType;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Евгений on 25.01.2017.
 */


public class ContactTest {

    private static Connection connection;
    private static ContactListDAO mscld;
    private String sqlForGetByLogin = "SELECT * FROM contactlist.contacts WHERE login=?;";
    private String sqlForDeleteByLogin = "DELETE FROM contactlist.contacts WHERE login=?;";

    @BeforeClass
    public static void init()
    {
        DAOFactory msdf = new MySQLDAOFactory();
        try {
            connection = msdf.getConnection();
            mscld = msdf.getContactListDAO(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testCreate()
    {
        Contact test = createTestRow();

        Contact testGet;
        testGet = getByLogin(test);
        Assert.assertNotNull(testGet);
        Assert.assertTrue(test.getName().equals(testGet.getName()));
        Assert.assertTrue(test.getSurname().equals(testGet.getSurname()));
        Assert.assertTrue(test.getLogin().equals(testGet.getLogin()));
        Assert.assertTrue(test.getEmail().equals(testGet.getEmail()));
        Assert.assertTrue(test.getPhoneNumber().equals(testGet.getPhoneNumber()));
        deleteByLogin(test);
    }

    @Test
    public void testUpdate()
    {
        Contact test = createTestRow();

        Contact testGet;

        Contact testUpd = new Contact();

        testUpd.setName("upd");
        testUpd.setSurname("upd");
        testUpd.setLogin(test.getLogin());
        testUpd.setEmail("upd");
        testUpd.setPhoneNumber("upd");
        mscld.update(testUpd);
        mscld.executeStatement(StatementType.Update);
        testGet = getByLogin(testUpd);

        Assert.assertNotNull(testGet);
        Assert.assertTrue(testUpd.getName().equals(testGet.getName()));
        Assert.assertTrue(testUpd.getSurname().equals(testGet.getSurname()));
        Assert.assertTrue(testUpd.getLogin().equals(test.getLogin()));
        Assert.assertTrue(testUpd.getLogin().equals(testGet.getLogin()));
        Assert.assertTrue(testUpd.getEmail().equals(testGet.getEmail()));
        Assert.assertTrue(testUpd.getPhoneNumber().equals(testGet.getPhoneNumber()));

        deleteByLogin(testUpd);


    }


    @Test
    public void testIsFoundLogin()
    {
        Contact test = createTestRow();
        boolean testIsFoundLogin = mscld.isFoundLogin(test.getLogin());
        Assert.assertTrue(testIsFoundLogin);
        deleteByLogin(test);
    }
    @Test
    public void testGetNumberOfRows()
    {
        Contact test = createTestRow();
        int a = mscld.getNumberOfRows();
        Assert.assertTrue(a > 0);
        deleteByLogin(test);

    }

    public Contact createTestRow()
    {
        Contact test = new Contact();
        test.setName("test");
        test.setSurname("test");
        test.setLogin("test");
        test.setEmail("test");
        test.setPhoneNumber("test");
        mscld.create(test);
        mscld.executeStatement(StatementType.Create);
        return test;
    }

    @Test
    public void testImportContact()
    {
        List<Contact> list;
        Contact test = createTestRow();
        ContactService contactService = ContactServiceImpl.getInstance();
        try {
            InputStream is = new FileInputStream("src/test//resources/testList.csv");
            contactService.importContact(is);
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        list = contactService.get("default",1,100);
        Contact newOne = list.get(list.size()-2);
        Contact newTwo = list.get(list.size()-1);

        Assert.assertTrue(newOne.getName().equals("new"));
        Assert.assertTrue(newOne.getSurname().equals("new"));
        Assert.assertTrue(newOne.getLogin().equals("new"));
        Assert.assertTrue(newOne.getEmail().equals("new"));
        Assert.assertTrue(newOne.getPhoneNumber().equals("new"));

        Assert.assertTrue(newTwo.getName().equals("new2"));
        Assert.assertTrue(newTwo.getSurname().equals("new2"));
        Assert.assertTrue(newTwo.getLogin().equals("new2"));
        Assert.assertTrue(newTwo.getEmail().equals("new2"));
        Assert.assertTrue(newTwo.getPhoneNumber().equals("new2"));

        Contact testGet = getByLogin(test);

        Assert.assertTrue(testGet.getName().equals("update"));
        Assert.assertTrue(testGet.getSurname().equals("update"));
        Assert.assertTrue(testGet.getLogin().equals(test.getLogin()));
        Assert.assertTrue(testGet.getEmail().equals("update"));
        Assert.assertTrue(testGet.getPhoneNumber().equals("update"));

        deleteByLogin(newOne);
        deleteByLogin(newTwo);
        deleteByLogin(testGet);
    }

    Contact getByLogin(Contact test)
    {
        Contact testGet = new Contact();
        try
        {
            PreparedStatement stm = connection.prepareStatement(sqlForGetByLogin);
            stm.setString(1,test.getLogin());
            stm.execute();
            ResultSet rs = stm.executeQuery();
            if (rs.next())
            {
                testGet.setId(rs.getLong("id"));
                testGet.setName(rs.getString("name"));
                testGet.setSurname(rs.getString("surname"));
                testGet.setEmail(rs.getString("email"));
                testGet.setLogin(rs.getString("login"));
                testGet.setPhoneNumber(rs.getString("phone_number"));
            }
            rs.close();
            stm.close();
        }

        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return testGet;
    }

    public void deleteByLogin(Contact contact)
    {
        try{
            PreparedStatement stm = connection.prepareStatement(sqlForDeleteByLogin);
            stm.setString(1,contact.getLogin());
            stm.execute();
            stm.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void closeResources()
    {
        try
        {
            connection.close();
            mscld.closeStatement(StatementType.Create);
            mscld.closeStatement(StatementType.Update);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
