<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Евгений
  Date: 09.01.2017
  Time: 12:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/text.css">
    <title>Просмотреть контакты</title>
</head>
<body>
Сортировка по:
<form action="/viewServlet" method="get">
    <p>
        <input type="radio" name="column" value="name"> Имя
        <input type="radio" name="column" value="surname"> Фамилия
        <input type="radio" name="column" value="login"> Логин
        <input type="radio" name="column" value="email"> E-mail
        <input type="radio" name="column" value="phone_number"> Номер телефона
        <input type="radio" name="column" value=id> По умолчанию</p>
        <input type="submit" name="apply" value="Применить">

</form>
<form action="/start" method="get">
    <input type="submit" name="apply" value="Вернуться на главную">
</form>

<table id="table-demo"   border="1px">
    <thead>
    <tr>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Логин</th>
        <th>e-mail</th>
        <th>Номер телефона</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${listResults}" var="user">
        <tr>
            <td>${user.name}</td>
            <td>${user.surname}</td>
            <td>${user.login}</td>
            <td>${user.email}</td>
            <td>${user.phoneNumber}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<ul id="pagination">
    <li class="previous-off"><a href="/viewServlet?page=prev">«Previous</a></li>
    <c:forEach begin="1" end="${numberOfPages}" var="i">
        <c:choose>
            <c:when test="${currentPage eq i}">
                <li class="active"><a href="#">${i}</a></li>
            </c:when>
            <c:otherwise>
                <li><a href="/viewServlet?page=${i}">${i}</a></li>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <li class="next-off"><a href="/viewServlet?page=next">Next »</a></li>
</ul>

<form action="/viewServlet" method="get">
    <select name = "numPerPage" >
        <option value = "title">Количество элементов на странице</option>
        <option value = "5">5</option>
        <option value = "10">10</option>
        <option value = "20">20</option>
    </select>
    <input type="submit" name="apply" value="Применить">
</form>

</body>
</html>
