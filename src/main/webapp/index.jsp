<%@ page import="domain.Contact" %>
<%@ page import="sql.MySQLDAOFactory" %>
<%@ page import="sql.MySQLContactListDAO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"  errorPage="error.jsp"%>
<html>
<head>
    <title>Список контактов</title>
    <link rel="stylesheet" type="text/css" href="css/text.css">
</head>
<body>
<h1>Список контактов</h1>
<form action="/viewServlet" method="get">
    <button class = "view" type="submit" name="viewContacts" value="">Посмотреть контакты</button>
</form>
<form action="importFile.jsp" method="post">
    <button class="import" type="submit" name="importFile">Импортировать контакты</button>
</form>

</body>
</html>