<%--
  Created by IntelliJ IDEA.
  User: Евгений
  Date: 13.01.2017
  Time: 12:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Произошла ошибка</title>
    <link rel="stylesheet" type="text/css" href="css/text.css">
</head>
<body>
<h1>
    Произошла ошибка! Происходит импорт контактов от другого пользователя.
    Пожалуйста подождите и попробуй снова!
</h1>
<form action="/start" method="get">
    <input type="submit" name="returnToIndex" value="Вернуться на главную">
</form>
</body>
</html>
