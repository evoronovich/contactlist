package dao;

import domain.Contact;
import sql.StatementType;

import java.util.List;

/**
 * Created by Евгений on 20.12.2016.
 */
public interface ContactListDAO {

    void create (Contact contact);
    void update (Contact contact);
    List<Contact> getAll (String paramSort, int numberOfPage, int totalPerPage);
    boolean isFoundLogin(String login);
    int getNumberOfRows();
    void closeStatement(StatementType type);
    void executeStatement(StatementType type);

}
