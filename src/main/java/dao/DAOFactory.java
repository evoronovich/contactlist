package dao;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Евгений on 20.12.2016.
 */
public interface DAOFactory {

    Connection getConnection() throws SQLException;
    ContactListDAO getContactListDAO(Connection connection);

}
