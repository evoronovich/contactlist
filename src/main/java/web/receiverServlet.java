package web;

import service.ContactService;
import service.ContactServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Евгений on 09.01.2017.
 */
@WebServlet("/importFile")
@MultipartConfig
public class receiverServlet extends Dispatcher {

    private boolean isFree = true;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (isFree) {
            isFree = false;
            Part filePart = request.getPart("file");
            InputStream filePartInputStream = filePart.getInputStream();
            ContactService contactService = ContactServiceImpl.getInstance();
            contactService.importContact(filePartInputStream);
            this.forward("/successImport.jsp",request,response);
            isFree = true;
        } else {
            this.forward("/error.jsp",request,response);
        }

    }


}

