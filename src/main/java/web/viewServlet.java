package web;

import domain.Contact;
import service.ContactService;
import service.ContactServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Евгений on 10.01.2017.
 */

@WebServlet("/viewServlet")
public class viewServlet extends Dispatcher {


    static AtomicInteger countReq = new AtomicInteger();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        int numPerPage;
        int currentPage;
        String paramSort;
        double dTotalPages;

        countReq.getAndIncrement();
        List<Contact> list;
        HttpSession session = request.getSession(true);

        if (session.getAttribute("currentPage") != null) {
            currentPage = (Integer) session.getAttribute("currentPage");
            paramSort =  (String) session.getAttribute("paramSort");
            numPerPage = (Integer) session.getAttribute("numPerPage");
        }
        else {
            currentPage = 1;
            paramSort = "id";
            numPerPage = 5;
        }


        ContactService contactService = ContactServiceImpl.getInstance();
        int totalRows = contactService.getNumberOfRows();

        if (request.getParameter("column") != null) {
            paramSort = request.getParameter("column");
            currentPage = 1;
        }


        if (request.getParameter("numPerPage") != null && !request.getParameter("numPerPage").equals("title")) {
            numPerPage = Integer.parseInt(request.getParameter("numPerPage"));
            currentPage = 1;
        }
        dTotalPages = (double) totalRows/numPerPage;
        int totalPages = (int) Math.ceil(dTotalPages);
        request.setAttribute("numberOfPages",totalPages);

        if (request.getParameter("page") != null) {
            if (request.getParameter("page").equals("prev")) {
                if (currentPage >1) {
                    currentPage--;
                }
            }
            else if (request.getParameter("page").equals("next")) {
                if (currentPage < totalPages) {
                    currentPage++;
                }
            }
            else {
                currentPage = Integer.parseInt(request.getParameter("page"));
            }

        }
        request.setAttribute("currentPage",currentPage);

        session.setAttribute("currentPage", currentPage);
        session.setAttribute("paramSort", paramSort);
        session.setAttribute("numPerPage", numPerPage);

        list = contactService.get(paramSort, currentPage, numPerPage);
        request.setAttribute("listResults",list);
        this.forward("/viewContacts.jsp",request,response);

        System.out.println(countReq);

    }



}
