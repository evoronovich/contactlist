package service;

import domain.Contact;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Евгений on 16.01.2017.
 */
public interface ContactService {
    void importContact(InputStream is);
    List<Contact> get(String param,int numberOfPage, int totalPerPage);
    int getNumberOfRows();

}
