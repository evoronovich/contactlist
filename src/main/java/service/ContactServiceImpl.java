package service;

import dao.ContactListDAO;
import dao.DAOFactory;
import domain.Contact;
import sql.MySQLDAOFactory;
import sql.StatementType;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Евгений on 21.12.2016.
 */
public class ContactServiceImpl implements ContactService {

    private static ContactService instance;


    private ContactServiceImpl() {
    }

    public static synchronized ContactService getInstance() {
        if (instance == null) {
            instance = new ContactServiceImpl();
        }
        return instance;
    }

    @Override
    public void importContact(InputStream is) {

        List<Contact> listContactToImport = read(is);
        DAOFactory msdf = new MySQLDAOFactory();
        int countBatchCreate = 0;
        int countBatchUpdate = 0;
        try(Connection connection = msdf.getConnection())
        {
            ContactListDAO mscld = msdf.getContactListDAO(connection);
            for (Contact element : listContactToImport) {
                if (countBatchCreate > 1000) {
                    mscld.executeStatement(StatementType.Create);
                    countBatchCreate = 0;
                }
                if (countBatchUpdate > 1000) {
                    mscld.executeStatement(StatementType.Update);
                    countBatchUpdate = 0;
                }

                if (mscld.isFoundLogin(element.getLogin())) {
                        mscld.update(element);
                        countBatchUpdate++;
                }
                else {
                mscld.create(element);
                countBatchCreate++;
                }
            }
            mscld.executeStatement(StatementType.Create);
            mscld.executeStatement(StatementType.Update);
            mscld.closeStatement(StatementType.Create);
            mscld.closeStatement(StatementType.Update);
            mscld.closeStatement(StatementType.IsFoundLogin);
        }

        catch (SQLException e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public List<Contact> get(String paramSort, int numberOfPage, int totalPerPage)   {
        int start;
        List<Contact> list = new ArrayList<>();
        ContactListDAO mscld;
        start = numberOfPage * totalPerPage - totalPerPage;

        try {
            DAOFactory msdf = new MySQLDAOFactory();
            mscld = msdf.getContactListDAO(msdf.getConnection());
            list = mscld.getAll(paramSort, start, totalPerPage);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    @Override
    public int getNumberOfRows ()
    {
        int numberOfRows = 0;
        DAOFactory msdf = new MySQLDAOFactory();
        ContactListDAO mscld;
        try {
            mscld = msdf.getContactListDAO(msdf.getConnection());
            numberOfRows = mscld.getNumberOfRows();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberOfRows;
    }


    private List<Contact> read(InputStream is)
    {
        List<Contact> list = new ArrayList<>();
        String s;
        String[] temp;

        try(BufferedReader bf = new BufferedReader(new InputStreamReader(is))) {
            try {
                while ((s = bf.readLine()) != null) {
                    Contact a = new Contact();
                    temp = s.split(",");
                    a.setName(temp[0]);
                    a.setSurname(temp[1]);
                    a.setLogin(temp[2]);
                    a.setEmail(temp[3]);
                    a.setPhoneNumber(temp[4]);
                    list.add(a);
                }
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

}
