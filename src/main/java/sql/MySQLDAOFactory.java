package sql;

import dao.DAOFactory;
import dao.ContactListDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Евгений on 20.12.2016.
 */
public class MySQLDAOFactory implements DAOFactory {


    private final String driver = "com.mysql.jdbc.Driver";
    private final String url = "jdbc:mysql://localhost:3306/contactlist?autoReconnect=true&useSSL=false";
    private final String user = "root";
    private final String password = "root";

    public MySQLDAOFactory() {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    @Override
    public ContactListDAO getContactListDAO(Connection connection) {
        return new MySQLContactListDAO(connection);
    }
}
