package sql;

import dao.ContactListDAO;
import domain.Contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Евгений on 20.12.2016.
 */
public class MySQLContactListDAO implements ContactListDAO {


    private final Connection connection;
    private static final String sqlForCreate = "INSERT INTO contacts (name, surname, login, email, phone_number) VALUES (?, ?, ?, ?,?);";
    private static final String sqlForUpdate = "UPDATE contacts SET  name=?, surname=?, email=?, phone_number=? WHERE login = ?;";
    private static final String sqlForIsFoundLogin = "SELECT * FROM contacts WHERE login=?;";
    private static final String sqlForGetNumberOfRows = "SELECT COUNT(*) AS rowcount FROM contacts;";
    private static final String sqlForGetAllSortedDefault = "SELECT * FROM contacts ORDER BY id LIMIT ?,?;";
    private static final String sqlForGetAllSortedName = "SELECT * FROM contacts ORDER BY name LIMIT ?,?;";
    private static final String sqlForGetAllSortedSurname = "SELECT * FROM contacts ORDER BY surname LIMIT ?,?;";
    private static final String sqlForGetAllSortedLogin = "SELECT * FROM contacts ORDER BY login LIMIT ?,?;";
    private static final String sqlForGetAllSortedEmail = "SELECT * FROM contacts ORDER BY email LIMIT ?,?;";
    private static final String sqlForGetAllSortedPhoneNumber = "SELECT * FROM contacts ORDER BY phone_number LIMIT ?,?;";

    private PreparedStatement stmForCreate;
    private PreparedStatement stmForUpdate;
    private PreparedStatement stmForGetNumberOfRows;
    private PreparedStatement stmForIsFoundLogin;
    private PreparedStatement stmForGetAll;


    public MySQLContactListDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create(Contact contact) {

        try  {
            if (stmForCreate == null) {
                stmForCreate = connection.prepareStatement(sqlForCreate);
            }
            stmForCreate.setString(1,contact.getName());
            stmForCreate.setString(2,contact.getSurname());
            stmForCreate.setString(3, contact.getLogin());
            stmForCreate.setString(4,contact.getEmail());
            stmForCreate.setString(5, contact.getPhoneNumber());
            stmForCreate.addBatch();
            stmForCreate.clearParameters();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void update(Contact contact) {

        try  {
            if (stmForUpdate == null) {
                stmForUpdate = connection.prepareStatement(sqlForUpdate);
            }
            stmForUpdate.setString(1, contact.getName());
            stmForUpdate.setString(2, contact.getSurname());
            stmForUpdate.setString(3, contact.getEmail());
            stmForUpdate.setString(4, contact.getPhoneNumber());
            stmForUpdate.setString(5, contact.getLogin());
            stmForUpdate.addBatch();
            stmForUpdate.clearParameters();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<Contact> getAll(String paramSort, int start, int totalPerPage) {
        ArrayList<Contact> list = new ArrayList<>();
        try {
            switch (paramSort) {
                case "id": stmForGetAll = connection.prepareStatement(sqlForGetAllSortedDefault);
                    break;
                case "name": stmForGetAll = connection.prepareStatement(sqlForGetAllSortedName);
                    break;
                case "surname": stmForGetAll = connection.prepareStatement(sqlForGetAllSortedSurname);
                    break;
                case "login": stmForGetAll = connection.prepareStatement(sqlForGetAllSortedLogin);
                    break;
                case "email": stmForGetAll = connection.prepareStatement(sqlForGetAllSortedEmail);
                    break;
                case "phone_number": stmForGetAll = connection.prepareStatement(sqlForGetAllSortedPhoneNumber);
                    break;
            }
            stmForGetAll.setInt(1,start);
            stmForGetAll.setInt(2,totalPerPage);
            ResultSet rs = stmForGetAll.executeQuery();
            while (rs.next()) {
                Contact c = new Contact();
                c.setId(rs.getLong("id"));
                c.setName(rs.getString("name"));
                c.setSurname(rs.getString("surname"));
                c.setEmail(rs.getString("email"));
                c.setLogin(rs.getString("login"));
                c.setPhoneNumber(rs.getString("phone_number"));
                list.add(c);
            }
            rs.close();
            stmForGetAll.clearParameters();

        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeStatement(StatementType.GetAll);
        }

        return list;
    }

    @Override
    public boolean isFoundLogin(String login) {

        boolean found = false;
        try {
            if (stmForIsFoundLogin == null) {
                stmForIsFoundLogin = connection.prepareStatement(sqlForIsFoundLogin);
            }
            stmForIsFoundLogin.setString(1,login);
            ResultSet rs = stmForIsFoundLogin.executeQuery();
            if (rs.next()) {
                found = true;
            }
            stmForIsFoundLogin.clearParameters();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }


    @Override
    public int getNumberOfRows() {
        int numberOfRows = 0;
        try {
            if (stmForGetNumberOfRows == null) {
                stmForGetNumberOfRows = connection.prepareStatement(sqlForGetNumberOfRows);
            }
            ResultSet rs = stmForGetNumberOfRows.executeQuery();
            if (rs.next()) {
                numberOfRows = rs.getInt("rowcount");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeStatement(StatementType.GetNumberOfRows);
        }
        return numberOfRows;
    }

    @Override
    public void closeStatement(StatementType type) {

        try {
            PreparedStatement stm = getStatement(type);
            if (stm != null )
            {
                stm.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private PreparedStatement getStatement(StatementType type) throws SQLException {
        switch (type) {
            case Create: return stmForCreate;
            case Update: return stmForUpdate;
            case GetNumberOfRows: return stmForGetNumberOfRows;
            case IsFoundLogin: return stmForIsFoundLogin;
            case GetAll: return stmForGetAll;
            default: return null;
        }
    }

    public void executeStatement (StatementType type)
    {
        try {
            PreparedStatement stm = getStatement(type);
            if (stm != null) {
                stm.executeBatch();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
