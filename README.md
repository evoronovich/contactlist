#### Contact List application ####

A simple web application, that allows you to import contact list to database from CSV file and view list of the imported contacts.

MENU:

* Menu page
* View list page
* Import page

CSV file contains information:

* Name
* Surname
* Login
* E-mail
* Phone number

It is not allowed to import files from different users simultaneously. Import works in two modes at the same time:

* Creating
* Updating

View mode supports paging and sorting by each column.

### Built with ###

* Java Servlet
* Maven
* JSP
* MySQL
* JUnit

Voice: +375298543875
E-mail: zhenek.zik@gmail.com